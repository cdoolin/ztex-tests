proc start_step { step } {
  set stopFile ".stop.rst"
  if {[file isfile .stop.rst]} {
    puts ""
    puts "*** Halting run - EA reset detected ***"
    puts ""
    puts ""
    return -code error
  }
  set beginFile ".$step.begin.rst"
  set platform "$::tcl_platform(platform)"
  set user "$::tcl_platform(user)"
  set pid [pid]
  set host ""
  if { [string equal $platform unix] } {
    if { [info exist ::env(HOSTNAME)] } {
      set host $::env(HOSTNAME)
    }
  } else {
    if { [info exist ::env(COMPUTERNAME)] } {
      set host $::env(COMPUTERNAME)
    }
  }
  set ch [open $beginFile w]
  puts $ch "<?xml version=\"1.0\"?>"
  puts $ch "<ProcessHandle Version=\"1\" Minor=\"0\">"
  puts $ch "    <Process Command=\".planAhead.\" Owner=\"$user\" Host=\"$host\" Pid=\"$pid\">"
  puts $ch "    </Process>"
  puts $ch "</ProcessHandle>"
  close $ch
}

proc end_step { step } {
  set endFile ".$step.end.rst"
  set ch [open $endFile w]
  close $ch
}

proc step_failed { step } {
  set endFile ".$step.error.rst"
  set ch [open $endFile w]
  close $ch
}

set_msg_config -id {HDL 9-1061} -limit 100000
set_msg_config -id {HDL 9-1654} -limit 100000

start_step init_design
set rc [catch {
  create_msg_db init_design.pb
  debug::add_scope template.lib 1
  set_property design_mode GateLvl [current_fileset]
  set_property webtalk.parent_dir C:/ztex/ztex-tests/ez_parallel_to_serial/fpga/fpga.cache/wt [current_project]
  set_property parent.project_path C:/ztex/ztex-tests/ez_parallel_to_serial/fpga/fpga.xpr [current_project]
  set_property ip_repo_paths c:/ztex/ztex-tests/ez_parallel_to_serial/fpga/fpga.cache/ip [current_project]
  set_property ip_output_repo c:/ztex/ztex-tests/ez_parallel_to_serial/fpga/fpga.cache/ip [current_project]
  add_files -quiet C:/ztex/ztex-tests/ez_parallel_to_serial/fpga/fpga.runs/synth_1/ez_p2s.dcp
  read_xdc C:/ztex/ztex-tests/ez_parallel_to_serial/fpga/fpga.srcs/constrs_1/ez_p2s.xdc
  link_design -top ez_p2s -part xc7a35tcsg324-1
  close_msg_db -file init_design.pb
} RESULT]
if {$rc} {
  step_failed init_design
  return -code error $RESULT
} else {
  end_step init_design
}

start_step opt_design
set rc [catch {
  create_msg_db opt_design.pb
  catch {write_debug_probes -quiet -force debug_nets}
  opt_design 
  write_checkpoint -force ez_p2s_opt.dcp
  catch {report_drc -file ez_p2s_drc_opted.rpt}
  close_msg_db -file opt_design.pb
} RESULT]
if {$rc} {
  step_failed opt_design
  return -code error $RESULT
} else {
  end_step opt_design
}

start_step place_design
set rc [catch {
  create_msg_db place_design.pb
  catch {write_hwdef -file ez_p2s.hwdef}
  place_design 
  write_checkpoint -force ez_p2s_placed.dcp
  catch { report_io -file ez_p2s_io_placed.rpt }
  catch { report_utilization -file ez_p2s_utilization_placed.rpt -pb ez_p2s_utilization_placed.pb }
  catch { report_control_sets -verbose -file ez_p2s_control_sets_placed.rpt }
  close_msg_db -file place_design.pb
} RESULT]
if {$rc} {
  step_failed place_design
  return -code error $RESULT
} else {
  end_step place_design
}

start_step route_design
set rc [catch {
  create_msg_db route_design.pb
  route_design 
  write_checkpoint -force ez_p2s_routed.dcp
  catch { report_drc -file ez_p2s_drc_routed.rpt -pb ez_p2s_drc_routed.pb }
  catch { report_timing_summary -warn_on_violation -max_paths 10 -file ez_p2s_timing_summary_routed.rpt -rpx ez_p2s_timing_summary_routed.rpx }
  catch { report_power -file ez_p2s_power_routed.rpt -pb ez_p2s_power_summary_routed.pb }
  catch { report_route_status -file ez_p2s_route_status.rpt -pb ez_p2s_route_status.pb }
  catch { report_clock_utilization -file ez_p2s_clock_utilization_routed.rpt }
  close_msg_db -file route_design.pb
} RESULT]
if {$rc} {
  step_failed route_design
  return -code error $RESULT
} else {
  end_step route_design
}

start_step write_bitstream
set rc [catch {
  create_msg_db write_bitstream.pb
  write_bitstream -force ez_p2s.bit 
  catch { write_sysdef -hwdef ez_p2s.hwdef -bitfile ez_p2s.bit -meminfo ez_p2s.mmi -ltxfile debug_nets.ltx -file ez_p2s.sysdef }
  close_msg_db -file write_bitstream.pb
} RESULT]
if {$rc} {
  step_failed write_bitstream
  return -code error $RESULT
} else {
  end_step write_bitstream
}

