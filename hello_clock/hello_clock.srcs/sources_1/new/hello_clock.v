`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 08/28/2015 02:37:03 PM
// Design Name: 
// Module Name: hello_clock
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module hello_clock(
    input fxclk,
    output reg [9:0] led1
    // input fxclk
    );
    
    integer counter;
    initial begin
        counter <= 0;
        led1 <= 10'b0100001011;
    end
    
    // assign led1 = 10'b0100001011;
    
    always @(posedge fxclk) begin
        // run on every positive edge of fx2 cloxk
        // clock likely at 48 MHz
        if (counter > 24000000) begin
            // every 24e6 clock cycles step leds.  eg. every .5 s
            led1[9:1] <= led1[8:0];
            led1[0] <= led1[9];
            counter <= 0;
        end else begin
            counter <= counter + 1;
        end
           
    end
endmodule
