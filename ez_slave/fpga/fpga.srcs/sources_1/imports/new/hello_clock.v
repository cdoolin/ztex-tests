`timescale 1ns / 1ps
//////////////////////////////////////////////////////////////////////////////////
// Company: 
// Engineer: 
// 
// Create Date: 08/28/2015 02:37:03 PM
// Design Name: 
// Module Name: hello_clock
// Project Name: 
// Target Devices: 
// Tool Versions: 
// Description: 
// 
// Dependencies: 
// 
// Revision:
// Revision 0.01 - File Created
// Additional Comments:
// 
//////////////////////////////////////////////////////////////////////////////////


module ez_slave(
    input wire [7:0] porta,
    output wire [7:0] led1
    );
    
    assign led1 = porta;
endmodule
